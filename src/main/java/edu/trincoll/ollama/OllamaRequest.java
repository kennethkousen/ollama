package edu.trincoll.ollama;

public record OllamaRequest(String model,
                            String prompt,
                            boolean stream) {
}
