package edu.trincoll.ollama;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalDateTime;

public record OllamaResponse(String model,
                             @JsonProperty("created_at") LocalDateTime createdAt,
                             String response,
                             boolean done) {}
