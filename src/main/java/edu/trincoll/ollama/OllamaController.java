package edu.trincoll.ollama;

import org.springframework.web.bind.annotation.*;

@RestController
public class OllamaController {

    private final OllamaService ollamaService;

    public OllamaController(OllamaService ollamaService) {
        this.ollamaService = ollamaService;
    }

    // localhost:8080/?model=llama2&prompt=Why%20is%20the%20sky%20blue%3F
    @GetMapping
    public String generate(@RequestParam(defaultValue = "llama2") String model,
                           @RequestParam String prompt) {
        return ollamaService.getResponse(model, prompt);
    }

    // Provide a JSON version of the OllamaRequest inside the body of the POST request
    @PostMapping
    public OllamaResponse generatePost(@RequestBody OllamaRequest request) {
        return ollamaService.generate(request);
    }

}
