package edu.trincoll.ollama;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OllamaService {

    private final OllamaInterface ollamaInterface;

    @Autowired
    public OllamaService(OllamaInterface ollamaInterface) {
        this.ollamaInterface = ollamaInterface;
    }

    public OllamaResponse generate(OllamaRequest request) {
        return ollamaInterface.generate(request);
    }

    public String getResponse(String model, String prompt) {
        var request = new OllamaRequest(model, prompt, false);
        OllamaResponse response = generate(request);
        System.out.println(response);
        return response.response();
    }

}
