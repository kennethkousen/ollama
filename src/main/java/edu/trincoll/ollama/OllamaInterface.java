package edu.trincoll.ollama;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.service.annotation.PostExchange;

public interface OllamaInterface {

    @PostExchange("/api/generate")
    OllamaResponse generate(@RequestBody OllamaRequest request);
}
