package edu.trincoll.ollama;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class OllamaControllerTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    void generate() {
        var ollamaResponse = restTemplate.getForObject(
                "/?model={model}&prompt={prompt}", String.class,
                "llama2", URLEncoder.encode("Why is the sky blue?", StandardCharsets.UTF_8));
        System.out.println(ollamaResponse);
        assertThat(ollamaResponse).contains("scattering");
    }

    @Test
    void generatePost() {
        var request = new OllamaRequest("llama2", "Why is the sky blue?", false);
        var ollamaResponse = restTemplate.postForObject("/", request, OllamaResponse.class);
        System.out.println(ollamaResponse);
        assertThat(ollamaResponse.response()).contains("scattering");
    }

}