package edu.trincoll.ollama;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class OllamaServiceTest {

    @Autowired
    private OllamaService ollamaService;

    @Test
    void getResponse() {
        String answer = ollamaService.getResponse("llama2", """
                Why is the sky blue?
                """);
        assertNotNull(answer);
        System.out.println(answer);
        assertThat(answer).containsIgnoringCase("scattering");
    }
}