# Spring Boot app to access Ollama API

This is a simple Spring Boot app that accesses the Ollama API. The app has a single endpoint that can take either a GET or a POST request:

* GET request: `http://localhost:8080/?model=llama2&prompt=hello`
* POST request: `http://localhost:8080/`

For the GET request, the prompt needs to be URL encoded (e.g., `http://localhost:8080/?model=llama2&prompt=hello%20world`). For the POST request, the prompt needs to be sent the JSON form of an `OllamaRequest` in the body (e.g., `{"model": "llama2", "prompt": "Why is the sky blue?", "stream": false}`).

The GET request returns the `String` response. The POST request returns a complete `OllamaResponse` object.

Before running, be sure to:

1. Install Ollama (see [the Ollama web site](https://ollama.com/) for details)
2. Install the `llama2` model (or whichever models you plan to use locally). You can do this by running `ollama run llama2` (or `ollama run <model>` for other models) from a command prompt.

------

Note: The tests will fail on the CI/CD server until I figure out how to install Ollama into the container and have it run the `llama2` model.